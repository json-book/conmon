Name:          conmon
Epoch:         2
Version:       2.1.5
Release:       2
Summary:       Monitoring program for OCI container
License:       ASL 2.0
URL:           https://github.com/containers/conmon
Source0:       https://github.com/containers/conmon/archive/refs/tags/v%{version}.tar.gz
Source1:       https://github.com/cpuguy83/go-md2man/archive/v1.0.10.tar.gz

BuildRequires: glib2-devel gcc git libseccomp-devel systemd-devel systemd-libs make golang 
Requires: glib2 systemd-libs libseccomp

%description
Conmon is a monitoring program and communication tool between a container manager (like podman or CRI-O)
and an OCI runtime (like runc or crun) for a single container.

%prep
%autosetup 

tar -xf %SOURCE1

%build
GO_MD2MAN_PATH="$(pwd)%{_bindir}"
mkdir -p _build/bin $GO_MD2MAN_PATH tools/build
cd go-md2man-*
go build -mod=vendor -o ../_build/bin/go-md2man .
cp ../_build/bin/go-md2man $GO_MD2MAN_PATH/go-md2man
cp ../_build/bin/go-md2man ../tools/build/
export PATH=$GO_MD2MAN_PATH:$PATH
cd -
export CFLAGS="${RPM_OPT_FLAGS}"
export LDFLAGS="-pie -Wl,-z,relro -Wl,-z,now"
%make_build all

%install
make PREFIX=%{buildroot}%{_prefix} install install.crio

%files
%license LICENSE
%doc README.md
%{_bindir}/conmon
%{_libexecdir}/crio/conmon
%{_mandir}/man8/*

%changelog
* Thu Feb 02 2023 wenchaofan <349464272@qq.com> - 2:2.1.5-2
- update to 2.1.5
* Thu Jan 12 2023 xu_ping <xuping33@h-partners.com> - 2:2.1.2-2
- Fix build failure caused by missing go-md2man in tools directory.

* Sun Sep 04 2022 tianlijing <tianlijing@kylinos.cn> - 2:2.1.2-1
- update to 2.1.2

* Mon Mar 16 2022 liukuo <liukuo@kylinos.cn> - 2:2.1.0-1
- Upgrade version to 2.1.0

* Thu Sep 09 2021 lingsheng <lingsheng@huawei.com> - 2:2.0.2-4
- Set CFLAGS to build debug related rpm

* Mon May 31 2021 baizhonggui <baizhonggui@huawei.com> - 2:2.0.2-3
- Add gcc in BuildRequires

* Mon Dec 23 2019 shijian <shijian16@huawei.com> - 2:2.0.2-2
- Package init
